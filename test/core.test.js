const expect = require('expect.js')
const { Ikarus, ServiceProvider } = require('@ikarusjs/ikarus')
const { SequelizeServiceProvider, Model, Endpoint } = require('../src')

class CustomProvider extends ServiceProvider {
  register() {
    this.app.set('mysql', 'mysql://root:@localhost:3306/ikarus')
  }
}

class BaseApp extends Ikarus {
  providers() {
    return [
      CustomProvider,
      SequelizeServiceProvider
    ]
  }
}

describe('Package Sequelize', async () => {
  it('adds sequelizeClient to config', async () => {
    const app = await BaseApp.create()

    expect(app.get('sequelizeClient')).to.be.an('object')
  })

  describe('endpoint', () => {
    it('registers endpoint', async () => {
      class Car extends Model {
        static schema({ field }) {
          return {
            name: field.string()
          }
        }
      }
      const test_endpoint = class extends Endpoint {
        static get Model() {
          return Car
        }
      }
      class App extends BaseApp { endpoints() { return { '/cars': test_endpoint } } }
      const app = await App.create()

      expect(app.service('/cars')).to.be.an('object')
    })
  })
})
