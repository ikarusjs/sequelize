const { Model } = require('sequelize')
const field = require('./field')
const inflector = require('inflector-js')

module.exports = class extends Model {
  static generate(app) {
    return this.init(
      this.schema({ field }),
      {
        sequelize: app.get('sequelizeClient'),
        tableName: this.tableName(),
        modelName: this.name,
        ...this.options()
      }
    )
  }

  static setup({ endpoint }) {
    const emit = event => (model, options) => {
      if(options && options.raw === undefined) {
        endpoint.service.emit(event, model.dataValues)
      }
    }

    this.afterCreate(emit('created'))
    this.afterDestroy(emit('removed'))
    this.afterUpdate(emit('updated'))
    this.afterBulkCreate(emit('created'))
    this.afterBulkDestroy(emit('removed'))
    this.afterBulkUpdate(emit('updated'))
    this.afterSave(emit('updated'))
    this.afterUpsert(emit('updated'))
  }

  static schema() {
    return {}
  }

  static options() {
    return {}
  }

  static tableName() {
    return inflector.pluralize(this.name.toLowerCase())
  }
}
