module.exports = {
  Endpoint: require('./endpoint'),
  Model: require('./model'),
  field: require('./field'),
  SequelizeServiceProvider: require('./SequelizeServiceProvider')
}
