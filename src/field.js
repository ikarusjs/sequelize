const { INTEGER, STRING, DATE, DATEONLY, TEXT } = require('sequelize').DataTypes

module.exports = Object.freeze({
  id(name = 'id') {
    return {
      [name]: this.increments({ primaryKey: true })
    }
  },

  increments(options) {
    return Object.assign({
      autoIncrement: true,
    },
    this.integer(), options)
  },

  foreignKey(options) {
    return Object.assign({
      type: INTEGER,
      allowNull: false
    }, options)
  },

  string(options) {
    return Object.assign({
      type: STRING,
      allowNull: false,
    }, options)
  },

  text(options) {
    return Object.assign({
      type: TEXT,
      allowNull: false,
    }, options)
  },

  integer(options) {
    return Object.assign({
      type: INTEGER,
      allowNull: false
    }, options)
  },

  timestamp(options) {
    return Object.assign({
      type: DATE,
      allowNull: false
    }, options)
  },

  date(options) {
    return Object.assign({
      type: DATEONLY
    })
  },

  timestamps(options) {
    return {
      createdAt: this.timestamp(),
      updatedAt: this.timestamp()
    }
  }
})
