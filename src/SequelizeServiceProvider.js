const { ServiceProvider } = require('@ikarusjs/ikarus')
const Sequelize = require('sequelize');

module.exports = class SequelizeServiceProvider extends ServiceProvider {
  register() {
    const connectionString = this.app.get('mysql');
    const sequelize = new Sequelize(connectionString, {
      dialect: 'mysql',
      logging: false,
    });

    this.app.set('sequelizeClient', sequelize);
  }

  boot() {
    const models = this.app.get('sequelizeClient').models;
    for(const name in models) {
      if ('associate' in models[name]) {
        models[name].associate(models);
      }
    }
  }
}
