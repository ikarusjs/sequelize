const { Service } = require('feathers-sequelize');
const { Endpoint } = require('@ikarusjs/ikarus')

module.exports = class extends Endpoint {
  generateService() {
    this.constructor.Model.generate(this.app)
    const service = new Service({
      Model: this.constructor.Model,
      paginate: this.app.get('paginate'),
      Sequelize: this.app.get('sequelizeClient').Sequelize
    })
    this.constructor.Model.setup({ endpoint: this })
    return service
  }
}
